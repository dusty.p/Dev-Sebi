# !/usr/bin/python
# -*- coding: utf8 -*-

"""
App entry point.

Something meaningful here, eventually.
"""

import asyncio
import logging
import os
import sys
import shutil
from libneko import embeds

from discord.ext import commands


from .utils.utils import is_contributor_check
from .shared_libs.bot import SebiMachineBot

# Init logging to output on INFO level to stderr.
logging.basicConfig(level="INFO", format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

REBOOT_FILE = "sebimachine/config/reboot"

# If uvloop is installed, change to that eventloop policy as it
# is more efficient
try:
    # https://stackoverflow.com/a/45700730
    if sys.platform == "win32":
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
        logging.warning("Detected Windows. Changing event loop to ProactorEventLoop.")
    else:
        import uvloop

        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        del uvloop
except BaseException as ex:
    logging.warning(
        f"Could not load uvloop. {type(ex).__qualname__}: {ex};", "reverting to default impl."
    )
else:
    logging.info(f"Using uvloop for asyncio event loop policy.")


class Reboot:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Reboots the bot", hidden=True)
    async def reboot(self, ctx):
        """Closes the bot process and starts it again automatically"""
        if is_contributor_check(ctx) is False:
            return
        logging.info(f"Rebooting by order from `{ctx.author}`")
        await ctx.send(
            embed=embeds.Embed(
                description="Sebi-Machine is restarting.",
                colour=self.bot.embed_color,
                timestamp=None,
            )
        )
        shutil.rmtree(REBOOT_FILE, ignore_errors=True)
        with open(REBOOT_FILE, "w") as f:
            f.write(f"1\n{ctx.channel.id}")
        # noinspection PyProtectedMember
        os._exit(0)


client = SebiMachineBot()

client.add_cog(Reboot(client))

client.run(client.bot_secrets["bot-key"])
