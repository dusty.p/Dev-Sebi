#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from libneko.extras import superuser


class CommunitySuperuserCog(superuser.SuperuserCog):
    LOGGER = logging.getLogger('SuperuserCog')

    def __init__(self, bot):
        super().__init__()
        self.LOGGER.info('Granting access to exec to %s', ', '.join(map(str, bot.owner_list)))

    async def owner_check(self, ctx):
        can_run = ctx.author.id in ctx.bot.owner_list
        if can_run:
            self.LOGGER.info('Authenticated %s to run %s in %s#%s', ctx.author, ctx.message.content, ctx.guild, ctx.channel)
        else:
            self.LOGGER.warning('Unauthorised user %s was prevented from running %s in %s#%s as they '
                            'are not on the list of valid owners.',
                            ctx.author, ctx.message.content, ctx.guild, ctx.channel)
        return can_run


def setup(bot):
    bot.add_cog(CommunitySuperuserCog(bot))
