#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Handles incoming errors safely.

===

MIT License

Copyright (c) 2018 Neko404NotFound

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*giggl
"""
import aiohttp
import asyncio  # Async sleep
from datetime import datetime
import io
import platform
import logging
import sys
import traceback  # Traceback utils.

import discord  # Embeds

from libneko.pag import factory  # Paginator

from sebimachine.shared_libs import string

# Errors
import asyncpg
import discord.ext.commands.errors as dpyext_errors  # Errors for ext.
from sebimachine.utils.utils import NotContributor, NotStaff


class TypeSetMixin:
    """
    Set for types.

    We define A as being included in B if we find a match for A in B, or we
    find that A is derived from any type in B.
    """

    def __contains__(self, item):
        for contained_item in iter(self):
            if issubclass(item, contained_item):
                return True
        return False


class FrozenTypeSet(TypeSetMixin, frozenset):
    pass


class TypeSet(TypeSetMixin, set):
    pass


# Respond with a reaction.
ignored_errors = {
    dpyext_errors.CommandNotFound: "\N{BLACK QUESTION MARK ORNAMENT}",
    dpyext_errors.DisabledCommand: "\N{NO ENTRY SIGN}",
    dpyext_errors.CommandOnCooldown: "\N{SNOWFLAKE}",
    NotImplementedError: "\N{CONSTRUCTION SIGN}",
}

handled_errors = FrozenTypeSet(
    {
        Warning,
        dpyext_errors.CheckFailure,
        dpyext_errors.NotOwner,
        dpyext_errors.MissingRequiredArgument,
        dpyext_errors.BadArgument,
        dpyext_errors.BotMissingPermissions,
        dpyext_errors.MissingPermissions,
        dpyext_errors.NoPrivateMessage,
        dpyext_errors.TooManyArguments,
        discord.Forbidden,
        discord.NotFound,
        discord.HTTPException,
        aiohttp.ClientError,
        aiohttp.ClientResponseError,
        asyncpg.UniqueViolationError,
        NotStaff,
        NotContributor,
    }
)


class ErrorHandler:
    def __init__(self, bot):
        self.bot = bot

    async def _react_for_a_little_while(self, me, message, reaction, time=15):
        try:
            await message.add_reaction(reaction)
            await asyncio.sleep(time)
            await message.remove_reaction(reaction, me)
        except:
            pass

    async def handle_error(self, *, bot, cog=None, ctx=None, error, event_method=None):
        """Send your errors here to be handled properly."""
        # Discard any errors as a result of handling this error.
        try:
            await self.__handle_error(
                bot=bot, cog=cog, ctx=ctx, error=error, event_method=event_method
            )

        except BaseException as ex:
            ex.__cause__ = error
            traceback.print_exception(type(ex), ex, ex.__traceback__)

    async def __handle_error(self, *, bot, cog=None, ctx=None, error, event_method=None):
        # Print the traceback first. This means I still see the TB even if
        # something else breaks when outputting results to discord.
        tb = traceback.format_exception(type(error), error, error.__traceback__)

        if cog:
            rel_log = logging.getLogger(type(cog).__name__)
        else:
            rel_log = self.bot.logger

        rel_log.warning("An error was handled.\n" + "".join(tb).strip())

        cause = error.__cause__ or error

        # Don't reply to errors with cool downs. Just add a reaction and stop.
        if type(cause) in handled_errors:
            if type(cause) in [dpyext_errors.CheckFailure, NotContributor, NotStaff]:
                reply = "You can't run this command. Do you have permission to do this?"
            elif type(cause) == asyncpg.UniqueViolationError:
                reply = "There is already a bot in this server with that id"
            elif type(cause) == discord.HTTPException:
                reply = "Could not find a member with that id"
            else:
                reply = string.cap(str(cause))

            destination = ctx if ctx else bot.get_channel(471618803256786944)

            # Since I take some raw input from errors, this is just a safeguard
            # to ensure messages are never too long. If an error message is
            # anywhere near this size, then it is a stupid error anyway.

            await destination.send(string.trunc(reply))

        elif type(cause) in ignored_errors:
            if isinstance(cause, dpyext_errors.CommandOnCooldown):
                after = round(cause.retry_after)

                # Show a message for 10s or so explaining
                await ctx.send(
                    f"You're going too fast! Retry in {after} "
                    f"second{'s' if after - 1 else ''}.",
                    delete_after=10,
                )

                # Show a reaction until the cooldown has ended.
                return await self._react_for_a_little_while(
                    ctx.bot.user, ctx.message, ignored_errors[type(cause)], cause.retry_after
                )
            else:
                return await self._react_for_a_little_while(
                    ctx.bot.user, ctx.message, ignored_errors[type(cause)]
                )

        else:
            msg = (
                "```diff\n"
                "- Uh-oh. You broke it.\n"
                "+ The developers have been contacted and will fix this error as soon as possible\n"
                "```\n"
            )

            await ctx.send(msg)

            error_file = io.StringIO()

            error_file.write(f"{datetime.utcnow()} UTC {type(error).__qualname__}\n\n")

            error_file.write(f"Environment info:\n")
            error_file.write(f"Python Version {platform.python_version()}\n")

            error_file.write("\n\n")

            if ctx:
                error_file.write(f"Cog: {cog or ctx.cog}\n")
                error_file.write(f"Command: {ctx.command}\n")
                error_file.write(f"Command parent: {ctx.command.parent}\n")
                error_file.write(f"Command invoked with: {ctx.invoked_with}\n")
                error_file.write(f"Prefix: {ctx.prefix}\n")
                error_file.write(f"Message ID: {ctx.message.id}\n")
                error_file.write(f"Author: {ctx.author} {ctx.author.id}\n")
                error_file.write(f"Guild: {ctx.guild} {ctx.guild.id if ctx.guild else None}\n")
                error_file.write(f"Channel: {ctx.channel} {ctx.channel.id}\n")
                error_file.write(f"Created on: {ctx.message.created_at}\n")
                error_file.write(f"Edited on: {ctx.message.edited_at}\n\n")
                error_file.write(f"Message payload:\n")
                error_file.write(f"{ctx.message.content!r}\n\n")
                error_file.write(f"Last HB Gateway Lat: {ctx.bot.latency}\n")

            else:
                error_file.write(f"Triggered by: {event_method} in {cog}\n\n")

            error_file.write("".join(tb))

            error_file.seek(0)

            p = factory.StringNavigatorFactory(prefix="```", suffix="```")
            p.add_lines(*[line for line in error_file.getvalue().split("\n")])
            try:
                destination = bot.get_channel(471618803256786944)
                for page in p.pages:
                    await destination.send(page)
            except Exception as ex:
                ex.__cause__ = error
                traceback.print_exception(type(ex), ex, ex.__traceback__)

    async def on_command_error(self, context, exception):
        """
        Handles invoking command error handlers.
        """
        # noinspection PyBroadException
        try:
            await self.handle_error(bot=self.bot, ctx=context, error=exception)
        except BaseException:
            traceback.print_exc()

    async def on_error(self, event_method, *_unused_args, **_unused_kwargs):
        """
        General error handling mechanism.
        """
        # noinspection PyBroadException
        try:
            _, err, _ = sys.exc_info()
            await self.handle_error(bot=self.bot, event_method=event_method, error=err)
        except BaseException:
            traceback.print_exc()


def setup(bot):
    bot.add_cog(ErrorHandler(bot))
