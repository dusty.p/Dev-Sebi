#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands
from libneko import embeds

from sebimachine.utils.utils import pretty_list
from sebimachine.utils.ioutils import in_here
from libneko import aiojson
import aiofiles
from libneko import aiojson


class Sar:
    def __init__(self, bot):
        self.bot = bot
        self.roles = bot.sar_roles

    ### OLD SAR ###
    # @commands.command(aliases=['selfrole', 'selfroles'])
    # async def sar(self, ctx, _type=None, _choice=None):
    #     # Examples:
    #     #    - ds!sar get 1 (adds Python Helper role)
    #     #    - ds!sar remove 3 (removes Other Helper role)
    #     #    - ds!sar list (shows all roles)
    #     #    - ds!sar (shows all roles)
    #     #    - ds!sar tst (sends a error message)

    #     def role_finder(role: str):
    #         return discord.utils.get(ctx.guild.roles, name=role)

    #     if _type == "list" or _type is None:
    #         em = embeds.Embed(title='List of Self Assigned Roles',
    #                            description='Usage: `ds!sar [ get | remove | list ] [ number ]`', colour=0x00FFFF)
    #         em.add_field(name='1. Python Helper', value='ds!sar get 1', inline=True)
    #         em.add_field(name='2. JS Helper', value='ds!sar get 2', inline=True)
    #         em.add_field(name='3. Other Helper', value='ds!sar get 3', inline=True)
    #         return await ctx.send(embed=em)

    #     roles = [role_finder('Python Helper'), role_finder('JS Helper'), role_finder('Other Helper')]

    #     if _choice not in ['1', '2', '3']:
    #         return await ctx.send('Enter a valid role number!')

    #     choice = int(_choice) - 1

    #     if _type == "get":
    #         await ctx.author.add_roles(roles[choice])
    #         await ctx.send('Added the role you requested')
    #     elif _type == "remove":
    #         await ctx.author.remove_roles(roles[choice])
    #         await ctx.send('Removed the role you requested')
    #     else:
    #         await ctx.send('Please provide a valid argument')
    #         await ctx.send('Please provide a valid argument')
    ### ###

    @commands.group(
        name="selfroles",
        brief="This provides some tools to tinker with self-assignable roles",
        case_insensitive=True,
    )
    async def self_roles_group(self, ctx):
        """
        Get, remove or list (or set or revoke if you are a contributor :) ) roles!
        This provides an easy and ituitive way of getting all the available roles that you desire!
        Check the subcommands help pages for more info.
        """
        if not ctx.invoked_subcommand:
            return await self.list_roles.callback(self, ctx)

    @self_roles_group.command(name="list", aliases=("l",), brief="Lists the available self-roles")
    async def list_roles(self, ctx):
        roles = {discord.utils.get(ctx.guild.roles, id=int(id)) for id in self.roles}
        pretty_roles = pretty_list({r.mention for r in roles}, sep="\n:small_blue_diamond:  ")
        if pretty_roles:
            pretty_roles = ":small_blue_diamond:  " + pretty_roles
        await ctx.send(
            embed=embeds.Embed(
                colour=ctx.bot.embed_color,
                title=":label: Self-obtainable roles for this guild",
                description=pretty_roles
                or "`There are no self-assignable roles for this guild yet`",
            )
        )

    @self_roles_group.command(
        name="get", aliases=("g",), brief="Assigns the given role, if there's one"
    )
    async def get_role(self, ctx, *, role: discord.Role):
        """
        Gets you the role you specified if it exists and is available!
        You can check wich roles are available for self-assigning by doing `ds!sar list` or simply `ds!sar`
        """
        try:
            if role.id in self.roles and role not in ctx.author.roles:
                await ctx.author.add_roles(role, reason="Self-assigned role")
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.check_mark_emoji} Successfuly assigned {role.mention}!",
                        timestamp=None,
                    )
                )
            else:
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.cross_mark_emoji} That role is either non-existent, not self-obtainable or you already have it!",
                        timestamp=None,
                    )
                )
        except discord.errors.Forbidden:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Missing the `MANAGE_ROLES` permission! Ask the admins to give me it first, then try again!",
                    timestamp=None,
                )
            )
        except:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Can't perform this action right now due to a databse error!",
                    timestamp=None,
                )
            )

    @self_roles_group.command(
        name="remove", aliases=("rm",), brief="Unassigns the given role, if there's one"
    )
    async def remove_role(self, ctx, *, role: discord.Role):
        """Removes the specified role from you if you have if, of course."""
        try:
            if role.id in self.roles and role in ctx.author.roles:
                await ctx.author.remove_roles(role, reason="Self-assigned role")
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.check_mark_emoji} Successfuly unassigned {role.mention}!",
                        timestamp=None,
                    )
                )
            else:
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.cross_mark_emoji} You either don't have that role or it's not self-obtainable!",
                        timestamp=None,
                    )
                )
        except discord.errors.Forbidden:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Missing the `MANAGE_ROLES` permission! Ask the admins to give me it first, then try again!",
                    timestamp=None,
                )
            )
        except:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Can't perform this action right now due to a databse error!",
                    timestamp=None,
                )
            )

    @commands.has_permissions(manage_roles=True)
    @self_roles_group.command(
        name="set", aliases=["s"], brief="Sets an existing role as a self-obtainable role"
    )
    async def set_self_role(self, ctx, *, role: discord.Role):
        """
        Registers the given role as self-assignable.
        This action can be undone with `ds!sar revoke <role>`.
        """
        try:
            async with aiofiles.open(in_here("config/sar.json")) as fp:
                json_data = await aiojson.load(fp)
                json_data["roles"].append(role.id)
                self.roles = json_data["roles"]
                aiojson.dump(json_data, fp)

            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.check_mark_emoji} Successfuly set {role.mention} as a self-obtainable role!",
                    timestamp=None,
                )
            )
        except:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Can't perform this action right now due to a databse error!",
                    timestamp=None,
                )
            )

    @commands.has_permissions(manage_roles=True)
    @self_roles_group.command(
        name="revoke", aliases=["rv"], brief="Revokes the self-role qualification from a self-role"
    )
    async def revoke_self_role(self, ctx, *, role: discord.Role):
        """
        Unregisters the given role as self-assignable.
        This action can be undone with `ds!sar set <role>`.
        """
        try:
            async with aiofiles.open(in_here("config/ids.json")) as fp:
                json_data = await aiojson.load(fp)
                json_data["roles"].pop(role.id)
                self.roles = json_data["roles"]
                aiojson.dump(json_data, fp)
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.check_mark_emoji} Successfuly revoked {role.mention} as a self-obtainable role!",
                    timestamp=None,
                )
            )
        except:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} Can't perform this action right now due to a databse error!",
                    timestamp=None,
                )
            )


def setup(bot):
    bot.add_cog(Sar(bot))
