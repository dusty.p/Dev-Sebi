#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands
import json
import aiofiles


class Tag:
    def __init__(self, bot):
        self.bot = bot
        with open("sebimachine/shared_libs/tags.json", "r") as fp:
            json_data = fp.read()
            global tags
            tags = json.loads(json_data)

    @commands.group(
        name="tag", brief="A tag/note system", case_insensitive=True, invoke_without_command=True
    )
    async def tag_group(self, ctx, tag: str = None):
        """
        This command group holds some commands that enable you to create and delete tags/notes.
        You can get a tag by specifying with this command.
        """
        await ctx.trigger_typing()
        if tag is None:
            return await ctx.send("Please provide a argument. Do `ds!help tag` for more info")

        found = tags.get(tag, None)

        if found is None:
            return await ctx.send("Tag not found")

        await ctx.send(found)

    @tag_group.command(name="list", brief="Lists the available tags", case_insensitive=True)
    async def list_tags(self, ctx):
        """Lists all the currently available tags"""
        await ctx.trigger_typing()
        desc = ""
        for i in tags:
            desc = desc + i + "\n"

        if desc == "":
            desc = "None"

        em = discord.Embed(title="Available tags:", description=desc, colour=self.bot.embed_color)

        await ctx.send(embed=em)

    @tag_group.command(
        name="add", aliases=("create",), brief="Adds a new tag to the list", case_insensitive=True
    )
    async def add_tag(self, ctx, tag_name=None, *, tag_info=None):
        """Adds a new tag"""
        await ctx.trigger_typing()
        if not ctx.author.guild_permissions.manage_roles:
            return await ctx.send("You are not allowed to do this")

        if tag_name is None or tag_info is None:
            return await ctx.send(
                "Please provide a tag name and the tag info. Do `help tag` for more info"
            )

        exists = False
        for i in tags:
            if i == tag_name:
                exists = True

        if not exists:
            tags.update({tag_name: tag_info})

            async with aiofiles.open("src/shared_libs/tags.json", "w") as fp:
                json_data = json.dumps(tags)
                await fp.write(json_data)

            return await ctx.send("The tag has been added")

        await ctx.send("The tag already exists")

    @tag_group.command(
        name="remove",
        aliases=("delete",),
        brief="Removes a tag from the list",
        case_insensitive=True,
    )
    async def remove_tag(self, ctx, tag=None):
        """Remove an existing tag"""
        await ctx.trigger_typing()
        if not ctx.author.guild_permissions.manage_roles:
            return await ctx.send("You are not allowed to do this")

        if tag is None:
            return await ctx.send(
                "Please provide a tag name and the tag info. Do `help tag` for more info"
            )

        found = None
        for i in tags:
            if i == tag:
                found = i

        if found is not None:
            del tags[found]
            async with aiofiles.open("src/shared_libs/tags.json", "w") as fp:
                json_data = json.dumps(tags)
                await fp.write(json_data)

            return await ctx.send("The tag has been removed")

        await ctx.send("The tag has not been found")


def setup(bot):
    bot.add_cog(Tag(bot))
