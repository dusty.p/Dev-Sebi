#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands

from libneko import embeds
from sebimachine.utils.utils import confirm_opts
from textwrap import dedent
from typing import Union

import re


prefix_re = re.compile(r"^\[([^\]]+)\]\s*(.+)$")


GREY_HUE = 0x363941
EMB_BOT_ID = "Bot ID"


class BotManager:
    def __init__(self, bot):
        self.bot = bot

    async def scrape_log(self, bot_user: discord.Member) -> Union[discord.Message, None]:
        """
        This will scrape the log channel and return a tuple containing a bool that represents the success of the scrape 
        and the message containing the invite for the passed bot account if it found one.
        """
        log_ch = bot_user.guild.get_channel(self.bot.channels["invite_log"])  # getting the channel
        async for msg in log_ch.history(
            limit=15
        ):  # I don't think there's need to search more than that
            if msg.embeds:  # Ignoring all messages that are not embeds
                emb = msg.embeds[
                    0
                ]  # Since all embeds in this channel are from Dev-Sebi there'll will only be 1 embed per msg
                for f in emb.fields:
                    if f.name == EMB_BOT_ID and f.value == f"`{bot_user.id}`":
                        return msg

    async def on_member_join(self, member: discord.Member):
        """
        This will handle the arrival of new bots.
        It will: 
            - Assign the correct role depending on the owner's role.
            - Give the proper nickname to the bot depending on the set prefix.
            - Give the bot dev the "Bot Developer" role
        """
        # If the member is not a bot
        if not member.bot:
            return

        # Check if the bot is in the db
        bot_data = await self.bot.db_con.fetchrow("SELECT * FROM bots WHERE id = $1", member.id)
        if not bot_data:
            return await member.kick(reason="The bot was not on the database")

        bot_owner = member.guild.get_member(bot_data["owner"])
        # Check if the bot is of a staff member and add the moderator role or admin role
        if (
            discord.utils.get(member.guild.roles, name="Administrator".casefold())
            in bot_owner.roles
        ):
            await member.add_roles(
                discord.utils.get(member.guild.roles, name="Admin Bots".casefold())
            )

        elif discord.utils.get(member.guild.roles, name="Moderator".casefold()) in bot_owner.roles:
            await member.add_roles(
                discord.utils.get(member.guild.roles, name="Moderator Bots".casefold())
            )

        # Add roles and change bots nick
        await member.add_roles(discord.utils.get(member.guild.roles, name="Bots"))
        await member.edit(nick=f'[{bot_data["prefix"]}] {member.display_name}')

        # Add role to the owner and send a message to the owner telling that the bot has been added
        await bot_owner.send(
            embed=embeds.Embed(
                description=dedent(
                    f"""
                Your bot, {member.name}, has been added to the server.
                You can use it in {self.bot.get_channel(self.bot.channels["bots"]["spam"]).mention}
                and {self.bot.get_channel(self.bot.channels["bots"]["spam"]).mention}.
                Enjoy! \N{WINKING FACE}
                """
                ),
                timestamp=None,
            )
        )
        await bot_owner.add_roles(
            discord.utils.get(member.guild.roles, name="Bot Developers".casefold())
        )

        # Adding the confirmation reaction to the invite log
        inv_log = await self.scrape_log(member)
        if inv_log:
            await inv_log.add_reaction(self.bot.check_mark_emoji)

    async def on_member_remove(self, member: discord.Member):
        """This will handle the removal of bots in case the bot or it's owner leaves or gets kicked."""
        if member.bot:
            await self.bot.db_con.execute("DELETE FROM bots WHERE id = $1", member.id)
        else:
            # Kick all of the member's bots
            bot_data = await self.bot.db_con.fetch("SELECT * FROM bots WHERE owner = $1", member.id)
            if not bot_data:
                # The user has no bots
                return

            values = [i[0] for i in [list(i.values()) for i in bot_data]]

            for v in values:
                bot = member.guild.get_member(v)
                await bot.kick(reason="Owner of this bot left or was kicked")

    async def on_member_ban(self, guild, user):
        """This will handle the removal of bots in case the bot or it's owner leaves or gets banned."""
        if user.bot:
            await self.bot.db_con.execute("DELETE FROM bots WHERE id = $1", user.id)
        else:
            # Ban all of the user's bots
            bot_data = await self.bot.db_con.fetch("SELECT * FROM bots WHERE owner = $1", user.id)
            if not bot_data:
                # The user has no bots
                return

            values = [i[0] for i in [list(i.values()) for i in bot_data]]

            for v in values:
                bot = guild.get_member(v)
                await bot.ban(reason="Owner of this bot was banned")

    @commands.command(name="invite", aliases=("in",), brief="Places an invite request for a bot")
    async def invite_bot(self, ctx, bot: int, prefix: str):
        """
        This will generate an invite request for the bot account with the given ID and prefix.
        However said request may take some time to be processed and so we ask you for your patience.
        The prefix must be at max. 6 characters long.
        It's important to note that you can change the prefix later, or kick your bot if you wish to do so.
        __**Note:**__ Your bots will be kicked or banned from the server if you leave/get kicked or get banned, respectively! 
        """
        bot = await ctx.bot.get_user_info(bot)
        if bot is None:
            return await ctx.send("That doesn't appear to be a valid ID.")

        # Make sure that the bot has not been invited already and it is not being tested
        if await self.bot.db_con.fetch("SELECT COUNT (*) FROM bots WHERE id = $1", bot.id) == 1:
            raise Warning(
                f"{self.bot.check_mark_emoji} The bot has already been invited or is being tested."
            )

        if len(prefix) > 6:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.check_mark_emoji} That prefix is too long! It must be 6 chars long at max.",
                    colour=self.bot.error_color,
                )
            )

        await self.bot.db_con.execute(
            "INSERT INTO bots (id, owner, prefix) VALUES ($1, $2, $3)",
            bot.id,
            ctx.author.id,
            prefix,
        )

        emb = embeds.Embed(colour=self.bot.embed_color, timestamp=None)
        emb.title = f"Hello {ctx.author.name},"
        emb.description = dedent(
            f"""
            Thanks for inviting {bot}! It will be invited shortly.
            Please open your DMs if they are not already so that I can contact
            you to inform you about the progress of this if need be.
            """
        )
        await ctx.send(embed=emb)

        emb = embeds.Embed(title="Bot Invite", colour=GREY_HUE)
        emb.description = discord.utils.oauth_url(bot.id, permissions=None, guild=ctx.guild)
        emb.set_thumbnail(url=bot.avatar_url)
        emb.add_field(name="Bot name", value=bot.name)
        emb.add_field(name=EMB_BOT_ID, value=f"`{bot.id}`")
        emb.add_field(name="Bot owner", value=ctx.author.mention)
        emb.add_field(name="Bot prefix", value=f"`{prefix}`")
        await ctx.bot.get_channel(self.bot.channels["invite_log"]).send(embed=emb)

    @commands.group(
        name="botmanager", aliases=["bm"], brief="Manage your bots!", invoke_without_subcommand=True
    )
    async def bot_manager_group(self, ctx):
        """
        This command groupd holds a variety of features related to bot management.
        You can use these commands to manage your bot claims, and do things like changing the prefix or kicking an old bot.
        """
        if ctx.invoked_subcommand is None:
            await self.claimed_bots.callback(ctx, ctx.author)

    @bot_manager_group.command(
        name="claim", aliases=("makemine", "gimme"), brief="Claims an unclaimed bot"
    )
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def claim_bot(self, ctx, bot: discord.Member, owner: discord.Member = None):
        """This will claim the ownership of a bot that doesn't have any user has it registered owner."""
        if not bot.bot:
            raise Warning(f"{self.bot.check_mark_emoji} You can only claim bots.")

        prefix_match = prefix_re.findall(bot.display_name)
        if prefix_match:
            prefix, _ = prefix_match[0]
        else:
            raise Warning(
                f"{self.bot.check_mark_emoji} Prefix can't be found in bot nick, please contact a staff member"
            )

        if owner is not None and ctx.author.guild_permissions.manage_roles:
            author_id = owner.id
        else:
            author_id = ctx.author.id

        emb = embeds.Embed(timestamp=None)

        if (
            await self.bot.db_con.fetchval("SELECT COUNT(*) FROM bots WHERE owner = $1", author_id)
            >= 10
        ):
            emb.colour = self.bot.error_color
            emb.title = "Too Many Bots Claimed"
            emb.description = dedent(
                """
                Each person is limited to claiming 10 bots as that is how
                many bots are allowed by the Discord API per user.
                """
            )
            return await ctx.send(embed=emb)
        bot_data = await self.bot.db_con.fetchrow("SELECT * FROM bots WHERE id = $1", bot.id)
        if not bot_data:
            await self.bot.db_con.execute(
                "INSERT INTO bots(id, owner, prefix) VALUES ($1, $2, $3)", bot.id, author_id, prefix
            )
            emb.colour = self.bot.embed_color
            emb.title = "Bot Claimed"
            emb.description = dedent(
                f"""
                You have claimed {bot.display_name} with a prefix of {prefix}\n
                fIf there is an error please run command again to correct the prefix
                for {ctx.prefix}unclaim {bot.mention} to unclaim the bot.
                """
            )
        elif bot_data["owner"] != author_id:
            emb.colour = self.bot.error_color
            emb.title = "Bot Already Claimed"
            emb.description = dedent(
                """
                This bot has already been claimed by someone else.
                If this is actually your bot please let the guild Administrators know.
                """
            )
        elif bot_data["owner"] == author_id:
            emb.colour = self.bot.embed_color
            emb.title = "Bot Already Claimed"
            emb.description = dedent(
                """
                You have already claimed this bot.
                If the prefix you provided is different from what is already in the database
                it will be updated for you.
                """
            )
            if bot_data["prefix"] != prefix:
                await self.bot.db_con.execute(
                    "UPDATE bots SET prefix = $1 WHERE id = $2", prefix, bot.id
                )
        elif not bot_data["owner"]:
            await self.bot.db_con.execute(
                "UPDATE bots SET owner = $1, prefix = $2 WHERE id = $3", author_id, prefix, bot.id
            )
            emb.colour = self.bot.embed_color
            emb.title = "Bot Claimed"
            emb.description = dedent(
                f"""
                You have claimed {bot.display_name} with a prefix of {prefix}\n' \
                If there is an error please run command again to correct the prefix,\n' \
                or {ctx.prefix}unclaim {bot.mention} to unclaim the bot.
                 """
            )
        else:
            emb.colour = self.bot.error_color
            emb.title = "Something Went Wrong..."

        await ctx.send(embed=emb)

    @bot_manager_group.command(
        name="unclaim", aliases=("ewwno",), brief="Unclaims a bot from your list"
    )
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def unclaim_bot(self, ctx, bot: discord.Member):
        """
        Unregisters you as the specifed bot account owner...


        *Don't you have a heart?? );*
        """
        if not bot.bot:
            raise Warning(f"{self.bot.check_mark_emoji} You can only unclaim bots.")

        emb = embeds.Embed(timestamp=None)

        bot_data = await self.bot.db_con.fetchrow("SELECT * FROM bots WHERE id = $1", bot.id)
        if not bot_data or not bot_data["owner"]:
            emb.colour = self.bot.error_color
            emb.title = "Bot Not Found"
            emb.description = "That bot is not claimed"
        elif bot_data["owner"] != ctx.author.id and not ctx.author.guild_permissions.manage_roles:
            emb.colour = self.bot.error_color
            emb.title = "Not Claimed By You"
            emb.description = dedent(
                """
                That bot is claimed by someone else.
                You can't unclaim someone else's bot
                """
            )
        else:
            await self.bot.db_con.execute("UPDATE bots SET owner = null WHERE id = $1", bot.id)
            emb.colour = self.bot.embed_color
            emb.title = "Bot Unclaimed"
            emb.description = dedent(
                f"""
                You have unclaimed {bot.display_name}
                If this is an error please reclaim using
                {ctx.prefix}claim {bot.mention} {bot_data["prefix"]}
                """
            )

        await ctx.send(embed=emb)

    @bot_manager_group.command(name="kick", aliases=["k"], brief="Kicks the given bot")
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def kick_bot(self, ctx, bot: discord.Member, reason: str = "Owner kicked it"):
        """
        This will attempt to kick the bot you choose, but it will prompt a confirmation screen first.
        I wouldn't let you kick a bot without knowing if you really mean it, of course...
        And you can specify a reason, I guess... Not that there's ever an excuse for rejecting a bot like that...


        *what a monster* :confounded:
        """
        bot_user = bot

        if not bot_user.bot:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} You can only manage bots!"
                )
            )

        owner_id = await self.bot.db_con.fetchvar(
            "SELECT owner FROM bots WHERE id = $1", bot_user.id
        )
        if owner_id != ctx.author.id:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} You are not this bot's owner!"
                )
            )

        confirm_m = await ctx.send(
            embed=embeds.Embed(
                title=f"Are you sure you want to kick {bot_user.name}?",
                description=f"Hit {self.bot.check_mark_emoji} if you are sure, and hit {self.bot.cross_mark_emoji} or wait 10 seconds if you want to cancel this action.",
                colour=0xF5BD23,  # nice yellow hue
            )
        )
        decision = await confirm_opts(self.bot, confirm_m)

        if decision:
            await confirm_m.clear_reactions()
            await bot_user.kick(reason=reason)
            await confirm_m.edit(
                embed=embeds.Embed(
                    title=f"Successfully kicked {bot_user.name}!",
                    description="You can still invite it back with the `invite` command if you wish to do so.",
                    colour=discord.Colour.green(),  # I like this green
                )
            )

    @bot_manager_group.command(
        name="changeprefix", aliases=["prefix", "pf"], brief="Change a bot's registered prefix"
    )
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def change_bot_prefix(
        self, ctx, bot: discord.Member, prefix: str, reason: str = "Owner changed its prefix"
    ):
        """
        This will change the registered prefix for the given bot, if it yours, obviously.
        The prefix must be 6 characters long at maximum.
        You can specify a reason if you want.
        """
        bot_user = bot

        if not bot_user.bot:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} You can only manage bots!"
                )
            )

        owner_id, old_prefix = await self.bot.db_con.fetchrow(
            "SELECT owner, prefix FROM bots WHERE id = $1", bot_user.id
        )
        if owner_id != ctx.author.id:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.cross_mark_emoji} You are not this bot's owner!"
                )
            )

        if len(prefix) > 6:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.check_mark_emoji} That prefix is too long! It must be 6 chars long at max.",
                    colour=self.bot.error_color,
                )
            )

        await bot_user.edit(nick=f"[{prefix}] {bot_user.name}", reason=reason)
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.check_mark_emoji} Successfully changed {bot_user.name} prefix from {old_prefix} to {prefix}"
            )
        )

    @commands.command(
        name="claims",
        aliases=("listclaims", "claimed", "bots"),
        brief="Lists the claims for the specified user",
    )
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def claimed_bots(self, ctx, user: discord.Member = None):
        """Shows a list of all the claimed bots that the given user has registered."""
        usr = user or ctx.author

        bots = await self.bot.db_con.fetch("SELECT * FROM bots WHERE owner = $1", usr.id)

        if bots:
            emb = embeds.Embed(
                title=f"{usr.display_name} has claimed the following bots:",
                colour=self.bot.embed_color,
                timestamp=None,
            )
            for bot in bots:
                emb.add_field(name=bot.display_name, value=f'Stored Prefix: {bot["prefix"]}')
        else:
            emb = embeds.Embed(
                title="You have not claimed any bots here yet.",
                colour=self.bot.embed_color,
                timestamp=None,
            )
        await ctx.send(embed=emb)

    @commands.command(
        name="owner",
        aliases=("whoowns", "whowns", "owns"),
        brief="Checks who owns the specifed bot",
    )
    async def bot_owner(self, ctx, bot: discord.Member):
        """
        Checks who's currently the registered owner of the specified bot. 
        Of course this may not be the actual owner of the bot account as it can be changed through the `claim` and `unclaim` commands.
        """
        if not bot.bot:
            return await ctx.send("This command is only for querying bots.")

        owner = await self.bot.db_con.fetchrow("SELECT * FROM bots WHERE id = $1", bot.id)

        if owner is None:
            await ctx.send(
                embed=embeds.Embed(
                    description="No one owns that bot!", colour=self.bot.error_color, timestamp=None
                )
            )
            await ctx.guild.get_channel(self.bot.channels["invite_log"]).send(
                embed=embeds.Embed(
                    description=f"No one appears to own {bot} ({bot.id})",
                    colour=self.bot.error_color,
                    timestamp=None,
                )
            )
        else:
            await ctx.send(
                embed=embeds.Embed(
                    description=ctx.guild.get_member(owner["owner"]).mention,
                    colour=GREY_HUE,
                    timestamp=None,
                )
            )


def setup(bot):
    bot.add_cog(BotManager(bot))
