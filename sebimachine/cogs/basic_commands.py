#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord
import asyncio
from sebimachine.utils import utils
from libneko import embeds

from textwrap import dedent
from time import monotonic as time_monotonic


class BasicCommands:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ping", brief="Pings the bot and gets the bot and ACK latency")
    async def ping_cmd(self, ctx):
        """
        A simple command to check if the bot is alive.
        It will answer with the bot and ACK latency
        """
        start = time_monotonic()
        msg = await ctx.send(embed=embeds.Embed(description="Pinging...", timestamp=None))
        millis = (time_monotonic() - start) * 1000
        heartbeat = ctx.bot.latency * 1000
        await msg.edit(
            embed=embeds.Embed(
                description=dedent(
                    f"""
            Pong! :ping_pong:
            **:heartbeat: Heartbeat:** {heartbeat:,.2f}ms\t**:file_cabinet: ACK:** {millis:,.2f}ms.
            """
                ),
                colour=self.bot.embed_color,
            )
        )

    @commands.command(
        name="tutorial",
        aliases=("guide", "tut"),
        brief="Starts a tutorial session that will guide you through this server!",
    )
    async def tutorial_cmd(self, ctx):
        """This command will start an interactive guide that can be very easy
        for newcomers to start learning their way in this server!"""
        TIMEOUT = 15

        emb = embeds.Embed(
            description=f"Hello, {ctx.author.display_name}. Welcome to Sebi's Bot Tutorials.\n"
            "First off, would you like a quick walkthrough on the server channels?",
            timestamp=None,
        )
        emb.set_footer(text=f"Waiting for {TIMEOUT} seconds...")
        await ctx.send(embed=emb)

        CHANNELS = self.bot.channels

        tut_channels = CHANNELS["tutorials"]

        bots_channels = tuple(self.bot.get_channel(CHANNELS[ch]) for ch in CHANNELS["bots"])

        help_channels = tuple(self.bot.get_channel(CHANNELS[ch]) for ch in CHANNELS["help"])

        def check(m):
            return m.author.id == ctx.author.id and m.channel.id == ctx.channel.id

        msg = await self.bot.wait_for("message", check=check, timeout=TIMEOUT)

        agree = (
            "yes",
            "yep",
            "non't",
            "ya",
            "ye",
            "yup",
            "ok",
            "why not",
            "yea",
            "yeah",
            "hell ye!",
            "heck ye!",
            "of course",
            "ofc",
            "let's go",
            "sure",
            "shall we?",
        )

        if msg is None:
            await ctx.send(
                embed=embeds.Embed(
                    description="Sorry, {ctx.author.mention}, you didn't reply on time. You can run the "
                    "command again when you're free :)",
                    timestamp=None,
                )
            )
        else:
            if msg.content.lower() in agree:
                async with ctx.typing():
                    await ctx.send(
                        embed=embeds.Embed(
                            description="Alrighty-Roo... Check your DMs!", timestamp=None
                        )
                    )
                    await ctx.author.send(
                        embed=embeds.Embed(description="Alrighty-Roo...", timestamp=None)
                    )

                    await asyncio.sleep(6)
                    await ctx.author.send(
                        embed=embeds.Embed(
                            description=f"To start making your bot from scratch, you first need to head over to {tut_channels['making-the-bot']}"
                            " (Regardless of the language you're gonna use).",
                            timestamp=None,
                        )
                    )

                    await asyncio.sleep(6)
                    await ctx.author.send(
                        embed=embeds.Embed(
                            description=f"After you have a bot account, you can continue with {tut_channels['dpy-rw']} "
                            f"if you want to make a bot in Discord.py Rewrite.\n\n But, if you want to make a bot made with Javascript and you don't like Python, consider using {tut_channels['js-klasa']},"
                            f"{tut_channels['d.js']} or even {tut_channels['js-com']}"
                            f"\nYou might prefer to do your bot in Java and for that we have {tut_channels['jda']}"
                            f"\n\nIf you already have old Discord.py async code and you want to use it with the new Rewrite versions, head to {tut_channels['a2r']}"
                            f"\n\nWe also have some other interesting tutorials, such as: Heroku ({tut_channels['heroku']}), MongoDB ({tut_channels['mongo']}),"
                            f"Redis ({tut_channels['redis']}) and SQL ({tut_channels['sql']}). Take a peak at those :D",
                            timestamp=None,
                        )
                    )

                    await asyncio.sleep(8)
                    await ctx.author.send(
                        embed=embeds.Embed(
                            description="\n\n\n...Read all the tutorials and still need help? You have two ways to get help.",
                            timestamp=None,
                        )
                    )
                    await asyncio.sleep(5)
                    await ctx.author.send(
                        embed=embeds.Embed(
                            description="**Method-1**\nThis is the best method of getting help. You help yourself.\n"
                            f"To do so, head over to a bots dedicated channel (either {bots_channels[0]} or {bots_channels[1]}, *and {bots_channels[2]} for testing music features*)"
                            " and type `?rtfm rewrite thing_you_want_help_with`.\nThis will trigger @R. Danny#6348 and will "
                            "give you links on your query on the official discord.py rewrite docs. *PS: Let the page completely load to get the auto-scroll*",
                            timestamp=None,
                        )
                    )

                    await asyncio.sleep(10)
                    await ctx.author.send(
                        embed=embeds.Embed(
                            description="**Method-2**\nIf you haven't found anything useful with the first method, feel free to ask your question "
                            f"in any of the related help channels. ({', '.join(help_channels)}  *phiew*)."
                            "\nJust choose the one that fits your question (make sure to read the channel topic)!"
                            "\nMay the force be with you!!",
                            timestamp=None,
                        )
                    )

            else:
                return await ctx.send(
                    embed=embeds.Embed(
                        description="Session terminated. You can run this command again whenever you want.",
                        timestamp=None,
                    )
                )


def setup(bot):
    bot.add_cog(BasicCommands(bot))
