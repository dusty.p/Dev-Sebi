#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
from datetime import datetime, timedelta
import textwrap
import logging
import discord
from libneko import embeds

GREEN_HUE = 0x51e723
RED_HUE = 0xfb3719

welcomer_log = logging.getLogger("welcomer")


class Welcomer:
    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def date_time_beautify(timestamp: datetime) -> str:
        """Outputs in the format YYYY-MM-DD at hh:mm:ss UTC"""
        return timestamp.strftime("%Y-%m-%d at %H:%M:%S %Z")

    @staticmethod
    def timespan_beautify(timespan: timedelta) -> str:
        total = int(timespan.total_seconds())

        mins, secs = divmod(total, 60)
        hours, mins = divmod(mins, 60)
        days, hours = divmod(hours, 24)
        months, days = divmod(days, 30)
        years, months = divmod(months, 12)

        bits = []
        years and bits.append(f'{years} year{years - 1 and "s" or ""}')
        months and bits.append(f'{months} month{months - 1 and "s" or ""}')
        days and bits.append(f'{days} day{days - 1 and "s" or ""}')
        hours and bits.append(f'{hours} hour{hours - 1 and "s" or ""}')
        mins and bits.append(f'{mins} minute{mins - 1 and "s" or ""}')
        secs and bits.append(f'{secs} second{secs - 1 and "s" or ""}')

        return ", ".join(bits[:3])

    async def on_member_join(self, member: discord.Member):
        """Handling a member join..."""
        welcomer_ch = self.bot.get_channel(int(self.bot.channels["welcomer"]))
        welcomer_log.info(f"Member Joined {member.display_name}")
        if member.bot:
            botowner = await self.bot.db_con.fetchval(
                "SELECT owner FROM bots WHERE id = $1", member.id
            )
            botownermember = member.guild.get_member(botowner)
            welcomer_log.info(f"{member.display_name} is a bot")
        desc = textwrap.dedent(
            f"""
            **Snowflake**: {member.id}
            **Mention**: {member.mention}
            **Account created at**: {self.date_time_beautify(member.created_at)}
            {"**Requested by**: " + botownermember.name if member.bot else ""}
            """
        )
        em = embeds.Embed(description=desc, colour=GREEN_HUE)
        welcomer_log.info(desc)

        em.set_author(
            name=f"{'BOT ' if member.bot else ''}{member} joined!", icon_url=member.guild.icon_url
        )
        em.set_footer(
            text=f"Member #{len(member.guild.members)} "
            f"(of which {sum([m.bot for m in member.guild.members])} are bots)"
        )
        em.set_thumbnail(url=member.avatar_url)

        await welcomer_ch.send(embed=em)
        welcomer_log.info("Welcome Message sent.")

    async def on_member_remove(self, member: discord.Member):
        """Handling the a member removal..."""
        welcomer_ch = self.bot.get_channel(int(self.bot.channels["welcomer"]))
        # Theory:
        # When a user gets nerfed, the audit logs do not update as fast as the bot does, so we
        # don't get the most up-to-date nerf record when we query them.
        # Fix:
        # Sleep for a bit, I guess.
        await asyncio.sleep(4)

        # Unsure of whether empty reasons can ever be NoneType, and this is more symbolic.
        # NOT_APPLICABLE if this event didn't occur. Falsey if it did occur but no reason was given, else
        # a string explaining why.
        reason_type = "left"
        reason = None

        # Check if banned or kicked
        try:
            async for entry in member.guild.audit_logs():
                created_at = entry.created_at

                # Couldn't decide between 10 (could rejoin in that time) and 5 (could fail if the bot is lagging).
                if created_at < datetime.utcnow() - timedelta(seconds=7.5):
                    break

                # target could be any object, it may not have an ID. (Cite: the docs)
                elif entry.target is not None and getattr(entry.target, "id", -1) == member.id:
                    action = entry.action

                    if action is discord.AuditLogAction.ban:
                        reason_type = "ban"
                        reason = entry.reason
                        break
                    elif action is discord.AuditLogAction.kick:
                        reason_type = "kick"
                        reason = entry.reason
                        break

        except discord.Forbidden:
            pass

        member_str = str(member) + (" a.k.a " + member.nick if member.nick else "")

        if reason_type == "left":
            formatted_msg = f"{member_str} just left!"
        elif reason_type == "kick":
            formatted_msg = f"{member_str} was kicked!"
        elif reason_type == "ban":
            formatted_msg = f"{member_str} was banned!"
        else:
            assert False, f"Unrecognised reason_type {reason_type}"

        bot_owner = None
        if member.bot:
            owner_id = await self.bot.db_con.fetchval(
                "SELECT owner FROM bots WHERE id = $1", member.id
            )
            bot_owner = member.guild.get_member(int(owner_id))
            welcomer_log.info(f"{owner_id} - {bot_owner.name}")

        em = embeds.Embed(
            title=formatted_msg,
            colour=RED_HUE,
            description=(
                f"**Owner**: {bot_owner.mention if bot_owner else 'N/A'}\n"
                f"**Top role**: {member.top_role.mention if str(member.top_role) != '@everyone' else member.top_role}\n"
                f"**Mention**: @{member}\n"
                f"**Snowflake**: {member.id}\n"
                f"**Account created at**: {self.date_time_beautify(member.created_at)}\n"
                f"**Joined here at**: {self.date_time_beautify(member.joined_at)}\n"
                f"**Was here for**: {self.timespan_beautify(datetime.utcnow() - member.joined_at)}\n"
                f"{'**Requested By**: ' + bot_owner.name if member.bot else ''}"
            ),
        )

        if reason is not None:
            em.description = f"**Reason**: {reason}\n" + em.description

        em.set_author(
            name=f"{'BOT ' if member.bot else ''}{member} left!", icon_url=member.guild.icon_url
        )
        em.set_footer(
            text=f"{len(member.guild.members)} members remain "
            f"(of which {sum([m.bot for m in member.guild.members])} are bots)"
        )
        em.set_thumbnail(url=member.avatar_url)

        await welcomer_ch.send(embed=em)


def setup(bot):
    bot.add_cog(Welcomer(bot))
