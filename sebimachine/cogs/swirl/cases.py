#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import abc
import functools
import json
from typing import Any, Optional

from discord.ext import commands
import discord

from dataclasses import dataclass
from libneko import embeds

import datetime
from sebimachine.utils.ioutils import in_here


REASON_FIELD = "Reason"
NO_REASON_GIVEN_VALUE = "`No reason given`"
INFINITY = float("inf")
PossiblyInfiniteNumber = float


with open(in_here("config/ids.json")) as fp:
    json = json.load(fp)
    LOGS_CH = json["swirl_logs"]


@dataclass(eq=False, order=False, repr=True, frozen=True)
@functools.total_ordering
class MainCase(abc.ABC):
    """
    Abstract dataclass for a logged moderation action case.
    Hashing, equality checking, and ordering are all implemented purely considering
    the :attr:`id` field.
    """

    id: int
    executor: discord.Member
    target: discord.Member
    created_at: datetime.datetime
    reason: Optional[str] = None
    log_id: Optional[int] = None

    @abc.abstractmethod
    async def log(self, ctx: commands.Context) -> None:
        """Logs an entry to the logger channel..."""
        ...

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, other: Any) -> bool:
        return hasattr(other, "id") and self.id == other.id

    def __lt__(self, other: Any) -> bool:
        return hasattr(other, "id") and self.id < other.id


@dataclass()
class Kick(MainCase):
    booties: Optional[str] = None

    async def log(self, ctx: commands.Context) -> None:
        boots = self.booties or "an old pair of boots"
        log = embeds.Embed(
            title=":boot: Kick",
            description=f"{self.executor.mention} kicked {self.target} (ID: {self.target.id}) with {boots}!",
        )
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class Ban(MainCase):
    duration: PossiblyInfiniteNumber = None

    async def log(self, ctx: commands.Context) -> None:
        duration = self.duration or INFINITY
        log = embeds.Embed(
            title="<:ban_hammer:480415759848439818> Ban",
            description=f"{self.executor.mention} banned {self.target} (ID: {self.target.id}) for {duration} hour(s)!",
        )  # This is debatable
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class Mute(MainCase):
    duration: PossiblyInfiniteNumber = None

    async def log(self, ctx: commands.Context) -> None:
        duration = self.duration or INFINITY
        log = embeds.Embed(
            title=":mute: Mute",
            description=f"{self.executor.mention} muted {self.target.mention} (ID: {self.target.id}) for {duration} hour(s)!",
        )  # This is debatable
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class Unmute(MainCase):
    async def log(self, ctx: commands.Context) -> None:
        log = embeds.Embed(
            title=":unmute: Unmute",
            description=f"{self.executor.mention} unmuted {self.target.mention} (ID: {self.target.id})!",
        )
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class Strike(MainCase):
    amount: int

    async def log(self, ctx: commands.Context) -> None:
        log = embeds.Embed(
            title=":triangular_flag_on_post: Strike",
            description=f"{self.executor.mention} striked {self.target.mention} (ID: {self.target.id}) **{self.amount}** time(s)!",
        )
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class Pardon(MainCase):
    amount: int

    async def log(self, ctx: commands.Context) -> None:
        log = embeds.Embed(
            title=":flag_white: Strike",
            description=f"{self.executor.mention} pardoned {self.target.mention} (ID: {self.target.id}) **{self.amount}** strike(s)!",
        )
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)


@dataclass()
class MsgClear(MainCase):
    amount: int
    channel: discord.TextChannel
    criteria: Optional[str] = None

    async def log(self, ctx: commands.Context) -> None:
        log = embeds.Embed(
            title=":wastebasket: Message Clear",
            description=f"{self.executor.mention} cleared **{self.amount}** messages in {self.channel.mention}!",
        )
        log.add_field(name="Criteria", value=self.criteria or "`None, they all went to oblivion`")
        log.add_field(name=REASON_FIELD, value=self.reason or "`No reason given`")
        log.set_footer(text=f"Case #{self.id}")

        mod_log_ch = ctx.bot.get_channel(LOGS_CH)
        await mod_log_ch.send(embed=log)
