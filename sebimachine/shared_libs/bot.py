# !/usr/bin/python
# -*- coding: utf8 -*-

"""
Bot Class
"""
import asyncio
import json
import logging
import os
import sys
import traceback

import asyncpg.pool 
from discord.ext import commands
from libneko import embeds
from typing import Dict

from sebimachine.config.config import LoadConfigBot
from sebimachine.shared_libs.loggable import Loggable
from sebimachine.utils.ioutils import in_here


REBOOT_FILE = "sebimachine/config/reboot"

class SebiMachineBot(LoadConfigBot, Loggable):
    """This discord bot is dedicated to http://www.discord.gg/GWdhBSp"""

    def __init__(self):
        # Initialize and attach config / settings
        if len(sys.argv) < 2:
            raise RuntimeError("Please provide a config file location as the first argument.")

        super().__init__(*sys.argv[1:])

        with open(self.private_credential_path) as fp:
            self.bot_secrets = json.load(fp)

        self.db_conn: asyncpg.pool.Pool = asyncio.get_event_loop().run_until_complete(self.__acquire_db())

        self.failed_cogs_on_startup = {}
        self.book_emojis: Dict[str, str] = {
            "unlock": "🔓",
            "start": "⏮",
            "back": "◀",
            "hash": "#\N{COMBINING ENCLOSING KEYCAP}",
            "forward": "▶",
            "end": "⏭",
            "close": "🇽",
        }

        # Load plugins
        # Add your cog file name in this list
        cogs = []
        try:
            with open(os.path.join("sebimachine", "extensions.txt")) as cog_file:
                for line in cog_file.read().split("\n"):
                    line = line.strip()
                    if not line or line.startswith("#"):
                        continue
                    if line.startswith("*"):
                        line = "sebimachine.cogs." + line[1:]
                    cogs.append(line)
        except FileNotFoundError:
            self.logger.warning('No extensions.txt was found, no extensions will be loaded...')

        for cog in cogs:
            try:
                self.load_extension(cog)
                self.logger.info(f"Loaded: {cog}")
            except BaseException as ex:
                logging.exception(
                    f"Could not load {cog}", exc_info=(type(ex), ex, ex.__traceback__)
                )
                self.failed_cogs_on_startup[cog] = ex

        self.check(lambda ctx: not ctx.author.bot)

    async def __acquire_db(self):
        try:
            self.db_con = await asyncpg.create_pool(**self.bot_secrets["db-con"])
        except Exception as ex:
            self.logger.exception('Failed to connect to database, will continue without it.')
            del ex
        else:
            self.logger.info('Successfully connected to database: %r', self.db_con)

    async def on_ready(self):
        """On ready function"""
        self.maintenance and self.logger.warning("MAINTENANCE ACTIVE")
        if os.path.exists(REBOOT_FILE):
            with open(REBOOT_FILE, "r") as f:
                reboot = f.readlines()
            if int(reboot[0]) == 1:
                await self.get_channel(int(reboot[1])).send(
                    embed=embeds.Embed(
                        description="Sebi-Machine has restarted.",
                        colour=self.embed_color,
                        timestamp=None,
                    )
                )
                for cog, ex in self.failed_cogs_on_startup.items():

                    try:
                        tb = "".join(traceback.format_exception(type(ex), ex, ex.__traceback__))[
                            -1500:
                        ]
                        channel = self.get_channel(self.config["channels"]["error_log"])
                        await channel.send(
                            f"FAILED TO LOAD {cog} BECAUSE OF {type(ex).__name__}: {ex}"
                        )

                        pag = commands.Paginator(prefix="```py", suffix="```")
                        for line in tb.split("\n"):
                            pag.add_line(line)

                        for page in pag.pages:
                            await channel.send(page)
                    except Exception:
                        self.logger.exception("Error occurred handling load error", exc_info=True)
        with open(REBOOT_FILE, "w") as f:
            f.write(f"0")

    async def on_command(self, ctx):
        self.logger.info('Invoking %s in %s#%s', ctx.message.content, ctx.guild, ctx.channel)



    async def on_guild_join(self, guild):
        await guild.leave()
