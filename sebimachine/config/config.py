#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import json
from typing import Any, NoReturn, Union
import discord
from discord.ext import commands


DEFAULT_PRIVATE_CONFIG_PATH = 'sebimachine/config/PrivateConfig.json'


class LoadConfigBot(commands.Bot):
    """
    Bot-wide configuration

    If unspecified, credential_file_path will default to sebimachine/config/PrivateConfig.json
    """
    logger: logging.Logger

    def __init__(self, main_config_path: str, credential_file_path: str = DEFAULT_PRIVATE_CONFIG_PATH,
                 **kwargs: Any) -> None:
        try:
            with open(main_config_path) as fp:
                self.config = json.load(fp)
        except Exception as ex:
            # Kills application.
            self.__fall_over(main_config_path)

            # Shut lint up. Never actually gets reached...
            del ex
        else:
            self.logger.info("Loaded configuration from %s successfully", main_config_path)

        # Initialize config
        self.owner_list = self.config["ownerlist"]
        self.default_prefix = self.config["prefix"]
        self.version = self.config["version"]
        self.display_name = self.config["display_name"]
        self.maintenance = self.config["maintenance"]
        self.embed_color = discord.Colour(0x00FFFF)
        self.error_color = discord.Colour(0xFF0000)

        # We cannot fetch emojis until the bot has connected...
        # Thus, we use placeholders and spin up an event to deal with the
        # rest later. Addresses #67
        self.check_mark_emoji = "\N{THUMBS UP SIGN}"
        self.cross_mark_emoji = "\N{THUMBS DOWN SIGN}"

        self.main_guild = self.config["main_guild"]
        self.channels = self.config["channels"]
        self.sar_roles = self.config["sar_roles"]

        self.private_credential_path = credential_file_path

        super().__init__(**kwargs, command_prefix=self.default_prefix)
        # Must come _after_ super init
        self.listen("on_ready")(self.generate_resolve_emojis_coro())

    def generate_resolve_emojis_coro(self: Union[commands.Bot, "LoadConfigBot"]):
        async def resolve_emojis():
            self.check_mark_emoji = self.get_emoji(self.config["chk_mrk_emoji"])
            self.cross_mark_emoji = self.get_emoji(self.config["crs_mrk_emoji"])

        return resolve_emojis

    @classmethod
    def __fall_over(cls, path: str) -> NoReturn:
        cls.logger.exception("Failed to load configuration from %s: cannot continue. Shutting down.", path)
        exit(1)
