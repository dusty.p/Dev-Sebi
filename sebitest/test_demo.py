#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
We use the unittest module for writing unit tests. This has a basic mock framework built in.

A mock is an object that pretends to be a real implementation of something, such as your bot, your
database, or your grandmother's dog. Mocks have specs. A spec is the definition of the object, so what
it can do and what it provides us with.

Python in it's awesomeness does not support writing asyncio-based unit tests, so... you either have to
manage the event loop for each individual test which is aids, or you need something to handle that
for you.

asynctest is a module on PyPi that wraps unittest and provides the same functionality, but also
provides several utilities for asyncio. If you use this in place of unittest, you just get added
asyncio compatibility, so just use that.

Mockito is a Java library for mocking objects:

    public class SomeTest {
        private Bot bot;
        private Connection conn;

        @BeforeEach
        void setUp() throws Exception {
            // Makes a mock bot
            bot = Mockito.mock(Bot.class);

            // Mock connection
            conn = Mockito.mock(Connection.class);

            // When we call fetch on a database conn with some string, output a list of some
            // predefined results.
            when(conn.fetch(anyString()))
                     .thenAnswer(invocation -> new Record(912, "espy", 22));

            // When we call bot.acquireConn(), return a mock database conn
            when(bot.acquireConn())
                    .thenReturn(conn);
        }

        @Test
        public void tesThatTheDatabaseIsImplementedAsExpected() {
            Record record;
            try (Connection conn = bot.acquireConn()) {
                record = conn.fetch("SELECT * FROM foo WHERE bar = 12");
            }

            assertEquals("Espy", record[1]);
        }
    }

...mockito is a port of this to Python, and works quite similar. I only found it 20 minutes
ago so I am no expert. This allows you to write mocks with more detailed side effects.

You may want to stub something instead of mocking it. This is where you just give hardcoded logic
for the entire function, rather than mocking out the control flow inside. `pretend` is a good
library for this...


    # stubbed bot.
    botStub = pretend.stub({"get_user": {"name": "Espy"}})

    # Test using the stub.
    self.assertEquals("Espy", botStub.get_user(1234).name)

Unit tests are written inside a class that derives from TestCase, and contain methods called testXXX. These take no
parameters other than self.

To assert conditions, you use self. The object will contain a bunch of methods to test things like assertTrue,
assertEquals, assertSame, assertNotNull, etc.
"""
import asynctest.mock
import discord
import mockito

from sebimachine.shared_libs.bot import SebiMachineBot


class SomeDatabaseTest(asynctest.TestCase):
    async def test_using_python_mocks(self):
        bot: SebiMachineBot = asynctest.NonCallableMagicMock(spec=SebiMachineBot)

        emoji = bot.get_emoji(1223)

        bot.get_emoji.assert_called_once_with(1223)
        # would fail:
        # bot.get_emoji.assert_called_once_with(9999)

    async def test_using_mockito_mocks(self):
        bot: SebiMachineBot = mockito.mock(spec=SebiMachineBot)

        (mockito.when(bot).get_emoji(1223)
                .thenReturn(mockito.mock({'name': 'ok_hand'}, spec=discord.Emoji)))

        # Run the test
        emoji = bot.get_emoji(1223)
        self.assertEqual("ok_hand", emoji.name)

        # always unstub at the end
        mockito.unstub()

