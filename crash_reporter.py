# !/usr/bin/python
# -*- coding: utf8 -*-

import discord

import json
import os
import sys

from sebimachine.config.config import LoadConfig
from sebimachine.utils.ioutils import in_here


class CrashReporter(discord.Client, LoadConfig):
    def __init__(self):
        if len(sys.argv) < 2:
            raise RuntimeError('Please provide a config file location as the first argument.')
        
        discord.Client.__init__(self, status=discord.Status.invisible)
        LoadConfig.__init__(self, sys.argv[1])
        with open(in_here('sebimachine/config', 'PrivateConfig.json')) as fp:
            self.secrets = json.load(fp)

    async def on_ready(self):
        channel = self.get_channel(self.config["channels"]["error_log"])

        msg = await channel.send(f'Last time the bot exited, it did it with a error. <@&{self.config["dev_sebi_developer"]}>, '
        'please commit a fix, then react to this message with \N{OK HAND SIGN} for the bot to reboot', file=discord.File('.log', 'Crash Report'))

        await msg.add_reaction('\N{OK HAND SIGN}')

        def check(reaction, user):
            return reaction.message.id == msg.id and str(reaction) == '\N{OK HAND SIGN}' and user.id != self.user.id

        await self.wait_for('reaction_add', check=check)

        await channel.send('Rebooting...')
        # noinspection PyProtectedMember
        os._exit(0)


client = CrashReporter()

client.run(client.secrets['bot-key'])

